package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;

import java.util.Collection;

public interface IProjectService {
    Collection<Project> getList();

    void create(@NotNull Project project);

    Project updateProject(@NotNull String projectId, @NotNull String name, @NotNull String userId);

    void deleteProject(@NotNull String projectId, @NotNull final String userId);

    void deleteAllProject(String userId);

}
