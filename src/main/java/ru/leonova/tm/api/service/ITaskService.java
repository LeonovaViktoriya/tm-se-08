package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;

import java.util.Collection;

public interface ITaskService {

    void create(@NotNull final Task task, @NotNull final String userId);

    Collection<Task> getList();

    void updateTaskName(@NotNull String taskId, @NotNull  String taskName);

    void deleteTasksByIdProject(@NotNull String projectId, @NotNull String userId);

    boolean isEmptyTaskList();

    void deleteTask(@NotNull String taskId, @NotNull String userId);

//    Task getById(@NotNull String id);

    void deleteAllTask(String userRole);
}
