package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;

import java.util.Collection;
import java.util.Map;

public interface IUserService {
    User getCurrentUser();

    void setCurrentUser(User currentUser);

    void create(@NotNull User user);

    User authorizationUser(@NotNull String login, @NotNull String password);

    boolean isAuth();

    User getById(@NotNull String userId);

    Collection<User> getList();

    void adminRegistration(@NotNull String admin, @NotNull String admin1);

    String md5Apache(@NotNull String password);

}
