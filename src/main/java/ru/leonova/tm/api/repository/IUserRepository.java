package ru.leonova.tm.api.repository;

import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;

import java.util.Collection;
import java.util.Map;

public interface IUserRepository {
    void persist(User user);

    User findOne(String userId);

    Collection<User> findAll();

    void remove(User user);

    void removeAll();
}
