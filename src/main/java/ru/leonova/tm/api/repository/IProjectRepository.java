package ru.leonova.tm.api.repository;

import ru.leonova.tm.entity.Project;

import java.util.Collection;

public interface IProjectRepository {

    void persist(Project p);

    Collection<Project> findAll();

    Project findOne(String id);

    void merge(Project p);

    void remove(Project p);

    void removeAll();

    boolean isExist(String projectId);
}
