package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Task {

    private String name;
    private String projectId;
    private String taskId;
    private String userId;
    private String description;
    private Date dateStart;
    private Date dateEnd;

    public Task(String name, String prId, String userId) {
        this.name = name;
        this.projectId = prId;
        this.userId = userId;
        taskId = UUID.randomUUID().toString();
    }
}

