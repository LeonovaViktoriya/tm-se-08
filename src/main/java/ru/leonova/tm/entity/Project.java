package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Project {

    private  String name;
    private String projectId;
    private String description;
    private Date dateStart;
    private Date dateEnd;
    private String userId;

    public Project(String name, String userId) {
        this.name = name;
        this.userId = userId;
        projectId = UUID.randomUUID().toString();
    }
}
