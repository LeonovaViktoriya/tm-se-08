package ru.leonova.tm.command.system;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.enumerated.RoleType;

public final class HelpCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "help";
    }

    @Override
    public final String getDescription() {
        return "Show all commands";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() {
        for (final AbstractCommand command: serviceLocator.getCommands()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }
}
