package ru.leonova.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.leonova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "info maven";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute(){
        System.out.println("Manifest-Version: " + Manifests.read("Manifest-Version"));
        System.out.println("Bundle-Name: " + Manifests.read("Bundle-Name"));
        System.out.println(Manifests.read("Created-By"));
    }
}
