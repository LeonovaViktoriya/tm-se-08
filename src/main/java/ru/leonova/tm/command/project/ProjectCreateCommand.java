package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-p";
    }

    @Override
    public String getDescription() {
        return "Create project";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        if (!serviceLocator.getUserService().isAuth()){
            System.out.println("You are not authorized");
            return;
        }
        System.out.print("[CREATE PROJECT]\nINTER NAME: \n");
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        @NotNull final Project project = new Project(getScanner().nextLine(), userId);
        serviceLocator.getProjectService().create(project);
        System.out.println("Project created");
    }
}
