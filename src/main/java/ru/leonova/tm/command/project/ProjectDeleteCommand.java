package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;

import java.util.Collection;

public final class ProjectDeleteCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-p";
    }

    @Override
    public String getDescription() {
        return "Delete project with all his tasks";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        if (!serviceLocator.getUserService().isAuth()) {
            System.out.println("You are not authorized");
            return;
        }
        System.out.println("[DELETE PROJECT BY ID]\nList projects:");
        Collection<Project> projectCollection = serviceLocator.getProjectService().getList();
        int i = 0;
        for (Project project : projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
        }
        System.out.println("Enter id project:");
        @NotNull final User currUser = serviceLocator.getUserService().getCurrentUser();
        @NotNull final String projectId = getScanner().nextLine();
        serviceLocator.getProjectService().deleteProject(projectId, currUser.getUserId());
        serviceLocator.getTaskService().deleteTasksByIdProject(projectId, currUser.getUserId());
        System.out.println("Project with his tasks are removed");

    }
}
