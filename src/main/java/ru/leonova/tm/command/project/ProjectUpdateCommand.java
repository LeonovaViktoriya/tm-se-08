package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;

import java.util.Collection;

public final class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "up-p";
    }

    @Override
    public String getDescription() {
        return "Update name project";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE NAME PROJECT]\nList projects");
        @NotNull final Collection<Project> projectCollection = serviceLocator.getProjectService().getList();
        int i = 0;
        for (Project project : projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
        }
        System.out.println("Enter id project:");
        @NotNull final String projectId = getScanner().nextLine();
        System.out.println("Enter new name for this project:");
        @NotNull final String name = getScanner().nextLine();
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        serviceLocator.getProjectService().updateProject(projectId, name, userId);
    }
}
