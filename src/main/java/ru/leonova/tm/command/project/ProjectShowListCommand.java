package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;

import java.util.Collection;

public final class ProjectShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-p";
    }

    @Override
    public String getDescription() {
        return "Show project list";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        @NotNull final Collection<Project> projectCollection = serviceLocator.getProjectService().getList();
        int i=0;
        for (@NotNull final Project project : projectCollection) {
            if(serviceLocator.getUserService().getCurrentUser().getUserId().equals(project.getUserId())) {
                System.out.println(++i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
            }
        }
    }
}
