package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;

public final class ProjectDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-p";
    }

    @Override
    public String getDescription() {
        return "Delete all projects";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("["+getDescription().toUpperCase()+"]");
        @NotNull final String userCurrRole = serviceLocator.getUserService().getCurrentUser().getRoleType();
        serviceLocator.getProjectService().deleteAllProject(userCurrRole );
        serviceLocator.getTaskService().deleteAllTask(userCurrRole );
    }
}
