package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;

public final class UserRegistrationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "reg";
    }

    @Override
    public String getDescription() {
        return "Registration user";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute(){
        System.out.println("["+getDescription().toUpperCase()+"]\nEnter login:");
        String login = getScanner().nextLine();
        System.out.println("Enter password:");
        String password = getScanner().nextLine();
        password = serviceLocator.getUserService().md5Apache(password);
        User user = new User(login, password);
        serviceLocator.getUserService().create(user);
        System.out.println("User is registered");
    }
}
