package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;

public final class UserUpdateLoginCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "up-l";
    }

    @Override
    public String getDescription() {
        return "Update login";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("["+getDescription().toUpperCase()+"]");
        serviceLocator.getUserService().isAuth();
        User currentUser = serviceLocator.getUserService().getCurrentUser();
        if(currentUser.getLogin().isEmpty()) return;
        System.out.println("Enter new login");
        currentUser.setLogin(getScanner().nextLine());
        System.out.println("Login is updated");
    }
}
