package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

public final class UserShowCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "show-u";
    }

    @Override
    public String getDescription() {
        return "Viewing and editing the current user profile";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute(){
        if(!serviceLocator.getUserService().isAuth()) return;
        System.out.println("["+getDescription().toUpperCase()+"]");
        User user = serviceLocator.getUserService().getCurrentUser();
        System.out.println("User profile:\nLogin: " + user.getLogin() + ", Password: " + user.getPassword() + ", Id: " + user.getUserId() + ", Role type: " + user.getRoleType());
    }
}
