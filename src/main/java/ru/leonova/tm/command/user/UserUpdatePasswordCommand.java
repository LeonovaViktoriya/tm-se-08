package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-pass";
    }

    @Override
    public String getDescription() {
        return "Update user password";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("["+getDescription().toUpperCase()+"]");
        User currentUser = serviceLocator.getUserService().getCurrentUser();
        if(currentUser==null || currentUser.getPassword().isEmpty())return;
        System.out.println("Enter new password");
        String password = getScanner().nextLine();
        password = serviceLocator.getUserService().md5Apache(password);
        currentUser.setPassword(password);
        System.out.println("Password is updated");
    }
}
