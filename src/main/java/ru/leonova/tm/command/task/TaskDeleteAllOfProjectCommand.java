package ru.leonova.tm.command.task;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public final class TaskDeleteAllOfProjectCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-t-t-p";
    }

    @Override
    public String getDescription() {
        return "Delete all task of the selected project";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[DELETE ALL TASKS OF THE SELECTED PROJECT]\n");
        Collection<Project> projectCollection = serviceLocator.getProjectService().getList();
        int i=0;
        for (Project project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
        }
        System.out.println("Select ID project: ");
        String projectId = getScanner().nextLine();
        serviceLocator.getTaskService().deleteTasksByIdProject(projectId, serviceLocator.getUserService().getCurrentUser().getUserId());
        System.out.println("All tasks of this project are deleted");
    }
}
