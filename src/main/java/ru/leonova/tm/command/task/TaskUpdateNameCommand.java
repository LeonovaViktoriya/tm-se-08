package ru.leonova.tm.command.task;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public final class TaskUpdateNameCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "up-t";
    }

    @Override
    public String getDescription() {
        return "Update task name";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() {
        if (serviceLocator.getTaskService().isEmptyTaskList()) return;
        System.out.println("[UPDATE NAME TASK]\nList tasks:");
        Collection<Task> taskCollection = serviceLocator.getTaskService().getList();
        int i = 0;
        for (Task task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName());
        }
        System.out.println("Enter id task for rename:");
        String taskId = getScanner().nextLine();
        if (taskId == null || taskId.isEmpty()) return;
        System.out.println("enter new name for task:");
        String name = getScanner().nextLine();
        if (name == null || name.isEmpty()) return;
        serviceLocator.getTaskService().updateTaskName(taskId, name);
        System.out.println("Task modified like " + name);
    }
}
