package ru.leonova.tm.command.task;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public final class TaskShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-t";
    }

    @Override
    public String getDescription() {
        return "Show tasks list";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() {
        if(serviceLocator.getTaskService().isEmptyTaskList()){
            System.out.println("List tasks is empty");
            return;
        }
        System.out.println("[TASK LIST]");
        Collection<Task> taskCollection = serviceLocator.getTaskService().getList();
        int i = 0;
        for (Task task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName());
        }
    }
}
