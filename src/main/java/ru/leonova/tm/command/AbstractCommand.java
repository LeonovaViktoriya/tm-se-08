package ru.leonova.tm.command;

import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Scanner;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    public void setBootstrap(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract boolean secure();

    public abstract void execute() throws Exception;

    protected Scanner getScanner(){
        return new Scanner(System.in);
    }

    public  RoleType[] roles() {
        return null;
    }

}
