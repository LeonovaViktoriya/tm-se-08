package ru.leonova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.TaskRepository;
import ru.leonova.tm.repository.UserRepository;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.service.ProjectService;
import ru.leonova.tm.service.TaskService;
import ru.leonova.tm.service.UserService;

import java.util.*;

public final class Bootstrap implements ServiceLocator {

    private final Scanner scanner = new Scanner(System.in);
    private final IProjectRepository IProjectRepository = new ProjectRepository();
    private final ITaskRepository ITaskRepository = new TaskRepository();
    private final IUserRepository IUserRepository = new UserRepository();
    private final IProjectService IProjectService = new ProjectService(IProjectRepository);
    private final ITaskService ITaskService = new TaskService(ITaskRepository, IProjectRepository);
    private final IUserService IUserService = new UserService(IUserRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Bootstrap() {}

    @NotNull
    @Override
    public final List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public final IProjectService getProjectService() {
        return IProjectService;
    }

    @NotNull
    @Override
    public final IUserService getUserService() {
        return IUserService;
    }

    @NotNull
    @Override
    public final ITaskService getTaskService() {
        return ITaskService;
    }

    private void registry(@NotNull final AbstractCommand command) throws Exception {
        @NotNull final String cliCommand = command.getName();
        @NotNull final String cliDescription = command.getDescription();
        if (cliCommand.isEmpty()) throw new Exception("It is not enumerated");
        if (cliDescription.isEmpty()) throw new Exception("Not description");
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private boolean isCorrectCommand(@NotNull String command) {
        for (@NotNull final AbstractCommand c:commands.values()) {
            if (c.getName().equals(command)) {
                return true;
            }
        }
        return false;
    }

    private void registry(@NotNull final Class... classes) throws Exception {
        for (@NotNull final Class clazz : classes) {
            if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
            @NotNull final Object command = clazz.newInstance();
            @NotNull final  AbstractCommand abstractCommand = (AbstractCommand) command;
            registry(abstractCommand);
        }
    }

    public void init(@NotNull final Class... classes) throws Exception {
        if (classes.length == 0) throw new Exception("Empty list");
        registry(classes);
        initAdmin();
        start();
    }

    private void initAdmin() {
        try {
            IUserService.adminRegistration("admin", "admin");
        } catch (Exception e) {
            System.out.println("Something went wrong in initAdmin!");
            e.printStackTrace();
        }
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        @NotNull String command;
        do {
            command = scanner.nextLine();
            execute(command);
        } while (!command.equals("exit"));
    }

    private void execute(@NotNull final String command) throws Exception {
        if (command.isEmpty() || !isCorrectCommand(command)) return;
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        @NotNull final boolean secureCheck = !abstractCommand.secure() || (abstractCommand.secure() && IUserService.isAuth());
        if (secureCheck || IUserService.getCurrentUser() != null) {
            abstractCommand.execute();
        } else {
            System.out.println("Log in for this command");
        }
    }

}

