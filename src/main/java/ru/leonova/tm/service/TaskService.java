package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    final private ITaskRepository taskRepository;
    final private ru.leonova.tm.api.repository.IProjectRepository projectRepository;

    public TaskService(ITaskRepository taskRepository, ru.leonova.tm.api.repository.IProjectRepository iProjectRepository) {
        this.taskRepository = taskRepository;
        projectRepository = iProjectRepository;
    }

    @Override
    public void create(@NotNull final Task task, @NotNull final String userId) {
        if (!userId.isEmpty() && task.getUserId().equals(userId)) {
            taskRepository.merge(task);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getList() {
        return taskRepository.findAll();
    }

    @Override
    public void updateTaskName(@NotNull final String taskId, @NotNull final String taskName) {
        if (!taskName.isEmpty() || !taskId.isEmpty()) return;
        @NotNull final Task task = taskRepository.findOne(taskId);
        if(task!=null) task.setName(taskName);
    }

    @Override
    public void deleteTasksByIdProject(@NotNull final String projectId, @NotNull final String userId) {
        if(projectId.isEmpty() || userId.isEmpty())return;
        @NotNull final Project project = projectRepository.findOne(projectId);
        if (project!=null && project.getUserId().equals(userId)) {
            Collection<Task> taskCollection = taskRepository.findAll();
            taskCollection.removeIf(task -> task.getProjectId().equals(projectId));
        }
    }

    @Override
    public boolean isEmptyTaskList() {
        return taskRepository.findAll().isEmpty();
    }

    @Override
    public void deleteTask(@NotNull final String taskId, @NotNull final String userId) {
        @NotNull final Task task = taskRepository.findOne(taskId);
        if (task == null || task.getProjectId()==null || task.getProjectId().isEmpty()) return;
        @NotNull final Project project = projectRepository.findOne(task.getProjectId());
        @NotNull final String projectUserId = project.getUserId();
        if (!projectUserId.isEmpty() && project.getUserId().equals(userId)) taskRepository.remove(task);
    }

    @Override
    public void deleteAllTask(@NotNull final String userRole) {
        if (!userRole.isEmpty() && userRole.equalsIgnoreCase(RoleType.ADMIN.getRole())) taskRepository.removeAll();
    }

}
