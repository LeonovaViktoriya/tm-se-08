package ru.leonova.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public final class UserService extends AbstractService<User> implements IUserService {

    final private IUserRepository userRepository;
    private User currentUser;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(@NotNull final User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void create(@NotNull final User user) {
        user.setRoleType(RoleType.USER.getRole());
        userRepository.persist(user);
    }

    @Override
    public Collection<User> getList() {
        return userRepository.findAll();
    }

    @Override
    public User authorizationUser(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty() || password.isEmpty()) return null;
        @NotNull final Collection<User> userCollection = userRepository.findAll();
        for (@NotNull final User user : userCollection) {
            if (user.getLogin().equals(login) & user.getPassword().equals(md5Apache(password))) {
                currentUser = user;
                return currentUser;
            }
        }
        return null;
    }

    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public User getById(@NotNull final String userId) {
        if (userId.isEmpty()) return null;
        return userRepository.findOne(userId);
    }

    public String md5Apache(@NotNull final String password) {
        if(!password.isEmpty())return DigestUtils.md5Hex(password);
        return null;
    }

    public void adminRegistration(@NotNull final String login, @NotNull String password) {
        if(login.isEmpty() || password.isEmpty()) return;
        @NotNull final User admin = new User("admin", md5Apache("admin"));
        admin.setRoleType(RoleType.ADMIN.getRole());
        userRepository.persist(admin);
    }

}
