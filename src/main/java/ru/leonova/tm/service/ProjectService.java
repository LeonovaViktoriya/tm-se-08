package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    final private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public Collection<Project> getList() {
        return projectRepository.findAll();
    }

    @Override
    public void create(@NotNull final Project project) {
        @NotNull final String projectId = project.getProjectId();
        if (!projectId.isEmpty() && isExist(projectId)) projectRepository.merge(project);
        else projectRepository.persist(project);
    }

    private boolean isExist(@NotNull final String projectId) {
        if(projectId.isEmpty()) return false;
        return projectRepository.isExist(projectId);
    }

    @Override
    public Project updateProject(@NotNull final String projectId, @NotNull final String name, @NotNull final String userId){
        if(name.isEmpty() || projectId.isEmpty() || userId.isEmpty()) return null;
        @NotNull final Project project = projectRepository.findOne(projectId);
        if (project != null && project.getUserId().equals(userId)) {
            project.setName(name);
            return project;
        }
        return null;
    }

    @Override
    public void deleteProject(@NotNull final String projectId, @NotNull final String userId) {
        if(projectId.isEmpty() || userId.isEmpty()) return;
        Project project = projectRepository.findOne(projectId);
        if (project != null && project.getUserId().equals(userId)) projectRepository.remove(project);
    }

    @Override
    public void deleteAllProject(@NotNull final String userRole) {
        if(!userRole.isEmpty() && userRole.equals(RoleType.ADMIN.getRole())) projectRepository.removeAll();
    }

}
