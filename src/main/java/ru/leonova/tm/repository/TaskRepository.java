package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class TaskRepository extends AbstractRepository<Task> implements ru.leonova.tm.api.repository.ITaskRepository {

    final private Map<String, Task> taskMap = new LinkedHashMap();

    @Override
    public void persist(@NotNull final Task task) {
        if (task.getTaskId()!=null && !task.getTaskId().isEmpty()) taskMap.put(task.getTaskId(), task);
    }

    @Override
    public void merge(@NotNull final Task task){
        @NotNull final String taskId = task.getTaskId();
        if(!taskId.isEmpty() && isExist(taskId)){
            @NotNull final Task taskUpdate = taskMap.get(taskId);
            taskUpdate.setName(task.getName());
        }else persist(task);
    }

    @Override
    public boolean isExist(@NotNull final String taskId){
        if(!taskId.isEmpty())return taskMap.containsKey(taskId);
        else return false;
    }

    @Override
    public Collection<Task> findAll(){
        return taskMap.values();
    }

    @Override
    public Task findOne(@NotNull final String taskId) {
        if(!taskId.isEmpty()) return taskMap.get(taskId);
        else return null;
    }

    @Override
    public void remove(@NotNull final Task task) {
        if(task.getTaskId()!=null && !task.getTaskId().isEmpty()) taskMap.remove(task.getTaskId());
    }

    @Override
    public void removeAll() {
        taskMap.clear();
    }

}

