package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;

abstract class AbstractRepository<E> {

    abstract void persist(@NotNull E e);
    abstract void merge(@NotNull E e);
}
