package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class UserRepository extends AbstractRepository<User> implements ru.leonova.tm.api.repository.IUserRepository {

    final private Map<String, User> userMap = new LinkedHashMap();

    @Override
    public void persist(@NotNull final User user) {
        userMap.put(user.getUserId(), user);
    }

    @Override
    void merge(@NotNull final User user) {
        @NotNull final String userId = user.getUserId();
        if (!userId.isEmpty() && isExist(userId)) userMap.get(userId).setLogin(user.getLogin());
        else persist(user);
    }

    private boolean isExist(@NotNull final String userId) {
        if (!userId.isEmpty()) return userMap.containsKey(userId);
        return false;
    }

    @Override
    public User findOne(@NotNull final String userId) {
        if (!userId.isEmpty()) return userMap.get(userId);
        return null;
    }

    @Override
    public Collection<User> findAll() {
        return userMap.values();
    }

    @Override
    public void remove(@NotNull final User user) {
       if(!user.getUserId().isEmpty()) userMap.remove(user.getUserId());
    }

    @Override
    public void removeAll() {
        userMap.clear();
    }

}
