package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.entity.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    private final Map<String, Project> projectMap = new LinkedHashMap();

    @Override
    public void persist(@NotNull final Project project) {
        projectMap.put(project.getProjectId(), project);
    }

    @Override
    public Collection<Project> findAll() {
        return projectMap.values();
    }

    @Override
    public Project findOne(@NotNull final String projectId) {
        if(projectId.isEmpty()) return null;
        return projectMap.get(projectId);
    }

    private void updateProjectName(@NotNull final String projectId, @NotNull final String name) {
        Project project = projectId.isEmpty() ? null : projectMap.get(projectId);
        if(project != null && name.isEmpty()) project.setName(name);
    }

    public boolean isExist(@NotNull final String projectId){
        if(projectId.isEmpty()) return false;
        return projectMap.containsKey(projectId);
    }

    @Override
    public void merge(@NotNull final Project project) {
        for (@NotNull final Project project1 : projectMap.values()) {
            if (project1.getProjectId().equals(project.getProjectId())) {
                updateProjectName(project1.getProjectId(), project.getName());
            } else {
                persist(project);
            }
        }
    }

    @Override
    public void remove(@NotNull Project project) {
        if(project.getProjectId()!=null) projectMap.remove(project.getProjectId());
    }

    @Override
    public void removeAll() {
        projectMap.clear();
    }

}
